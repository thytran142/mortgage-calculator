import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getLabel'
})
export class GetLabelPipe implements PipeTransform {

  // @ts-ignore
  transform(value: string, options: {label: string, value: string}[]): string {
      const option = options.find(x => x.value === value);
      // @ts-ignore
    return option ? option.label : option;
  }

}
