import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plural'
})
export class PluralPipe implements PipeTransform {

  transform(value: number, singularLabel: string): string {
    if (value <= 1) {
      return value + ' ' + singularLabel;
    } else {
      return value + ' ' + singularLabel + 's';
    }
  }

}
