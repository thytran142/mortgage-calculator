import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MortgageService} from "./services/mortgage.service";
import {Mortgage} from "./model/mortgage";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = 'Canadian Mortgage Calculator';
  public resForm: FormGroup;
  public amortizationPeriodYears: number[] = [];
  public paymentFrequencyOptions: { label: string, value: string }[] = [];
  public terms: number[] = [];
  public mortgage: Mortgage;
  constructor(private formBuilder: FormBuilder,
              private mortgageService: MortgageService) {
  }
  createForm(): void {
    this.resForm = this.formBuilder.group({
      mortgageAmount: [100000, Validators.compose([Validators.required, Validators.min(0)])],
      interestRate: [5, Validators.compose([Validators.required, Validators.min(0)])],
      amortizationPeriodYear: [25, Validators.required],
      paymentFrequency: ['accelerate-weekly', Validators.required],
      term: [5, Validators.required],
      prePaymentAmount: [0, Validators.compose([Validators.required, Validators.min(0)])],
    });
    this.setUpDataForMortgageService();
    this.mortgage = this.mortgageService.calculateMortgage();
    this.resForm.valueChanges.subscribe(value => {
      if (this.resForm.valid) {
        this.setUpDataForMortgageService();
        this.mortgage = this.mortgageService.calculateMortgage();
      }
    });
  }
  setUpDataForMortgageService(): void {
    if (this.resForm.valid) {
      this.mortgageService.setUpData(this.resForm.get('mortgageAmount').value as number,
        this.resForm.get('interestRate').value as number,
        this.resForm.get('amortizationPeriodYear').value as number,
        this.resForm.get('paymentFrequency').value,
        this.resForm.get('term').value as number,
        this.resForm.get('prePaymentAmount').value as number)
    }
  }
  setUpAmortizationPeriodYears(): void {
    for (let i = 1; i < 31; i++) {
      this.amortizationPeriodYears.push(i);
    }
  }
  setUpTerms(): void {
    for (let i = 0; i <= 10; i++) {
      this.terms.push(i);
    }
  }
  setUpPaymentFrequencyOptions(): void {
    this.paymentFrequencyOptions.push({label: 'Accelerated Weekly', value: 'accelerate-weekly'});
    this.paymentFrequencyOptions.push({label: 'Weekly', value: 'weekly'});
    this.paymentFrequencyOptions.push({label: 'Accelerated Bi-weekly', value: 'accelerate-bi-weekly'});
    this.paymentFrequencyOptions.push({label: 'Bi-weekly (Every 2 weeks)', value: 'bi-weekly'});
    this.paymentFrequencyOptions.push({label: 'Semi-monthly (24x per year)', value: 'semi-monthly'});
    this.paymentFrequencyOptions.push({label: 'Monthly (12x per year)', value: 'monthly'});
  }
  ngOnInit(): void {
    this.setUpAmortizationPeriodYears();
    this.setUpPaymentFrequencyOptions();
    this.setUpTerms();
    this.createForm();
  }
}
