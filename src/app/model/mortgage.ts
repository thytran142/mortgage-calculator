export class Mortgage {
  numberOfPaymentsPerTerm: number;
  paymentPerTime: number;
  prePaymentAmount: number;
  totalInterestPaymentForTerm: number;
  totalCostForTerm: number;
  principalPaymentForTerm: number;
  savingInterestForTerm: number;
}
