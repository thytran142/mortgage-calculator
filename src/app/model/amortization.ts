export class Amortization {
  period: number;
  amortizationInterest: number;
  amortizationCapital: number;
  remainingCapital: number;
}
