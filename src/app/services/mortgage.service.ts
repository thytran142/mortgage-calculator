import {Injectable} from '@angular/core';
import {Mortgage} from "../model/mortgage";
import {AmortizationService} from "./amortization.service";
import {Amortization} from "../model/amortization";

@Injectable({
  providedIn: 'root'
})
export class MortgageService {
  private mortgageAmount: number;
  private interest: number;
  private amortizationPeriodYear: number;
  private paymentFrequency: string;
  private term: number;
  private initialDeposit: number;
  constructor(private amortizationService: AmortizationService) {
  }

  /**
   *
   * @param mortgageAmount
   * @param annualInterestRate
   * @param amortizationPeriodYear
   * @param paymentFrequency
   * @param term
   * @param prePaymentAmount
   */
  public setUpData(mortgageAmount: number, annualInterestRate: number, amortizationPeriodYear: number, paymentFrequency: string, term: number, prePaymentAmount: number): void {
    this.mortgageAmount = mortgageAmount;
    this.interest = annualInterestRate;
    this.amortizationPeriodYear = amortizationPeriodYear;
    this.paymentFrequency = paymentFrequency;
    this.term = term;
    this.initialDeposit = prePaymentAmount;
  }

  /**
   * Estimate mortgage for terms
   */
  public calculateMortgage(): Mortgage {
    const monthlyPayment = this.calculateMortgageMonthlyPayment();
    const perPayment = this.estimateMortgagePaymentByMonthlyPayment(monthlyPayment, this.paymentFrequency);
    const numberOfPeriod = this.calculateNumberOfPaymentPerTerm(this.term, this.paymentFrequency);
    const interestRate = this.calculateInterestRate(this.interest, this.getPaymentFrequencyPerYear(this.paymentFrequency));
    const mortgageWithoutDeposit = this.mortgageAmount;
    const interestAmortizationWithoutDeposit =  this.amortizationService.getAmortization(mortgageWithoutDeposit ,perPayment, numberOfPeriod, interestRate);
    const totalInterestWithoutDeposit = + this.getTotalInterest(interestAmortizationWithoutDeposit).toFixed(7);
    const mortgageTotal = this.mortgageAmount - this.initialDeposit;
    const interestAmortizationResultForTerm = this.amortizationService.getAmortization(mortgageTotal,perPayment, numberOfPeriod, interestRate);
    const totalInterestForTerm = this.getTotalInterest(interestAmortizationResultForTerm).toFixed(7);
    const totalCost = +(perPayment * numberOfPeriod).toFixed(7);
    const principalPaymentForTerm = totalCost - (+totalInterestForTerm);

    return {
      numberOfPaymentsPerTerm: numberOfPeriod,
      paymentPerTime: perPayment,
      prePaymentAmount: this.initialDeposit,
      totalInterestPaymentForTerm: +totalInterestForTerm,
      principalPaymentForTerm: +principalPaymentForTerm,
      totalCostForTerm: totalCost,
      savingInterestForTerm: totalInterestWithoutDeposit - (+totalInterestForTerm)
    };
  }

  /**
   *
   * @param amortizationResult
   */
  private getTotalInterest(amortizationResult: Amortization[]): number {
    let totalInterest = 0;
    amortizationResult.forEach(amortization => {
      console.log(amortization);
      totalInterest += amortization.amortizationInterest;
    });
    return totalInterest;
  }
  private calculateMortgageMonthlyPayment(): number {
    // M = P [ i(1 + i)^n ] / [ (1 + i)^n – 1]
    const monthlyInterestRate = this.calculateInterestRate(this.interest, 12);
    const numerator = monthlyInterestRate * Math.pow((1 + monthlyInterestRate), 12 * this.amortizationPeriodYear);
    const denominator = Math.pow((1 + monthlyInterestRate), 12 * this.amortizationPeriodYear) - 1;

    return +(this.mortgageAmount * (numerator / denominator)).toFixed(2);
  }

  /**
   *
   * @param annualInterestRate
   * @param paymentFrequencyPerYear
   */
  private calculateInterestRate(annualInterestRate: number, paymentFrequencyPerYear: number): number {
    return (annualInterestRate / 100) / paymentFrequencyPerYear;
  }

  /**
   *
   * @param paymentFrequency
   */
  private getPaymentFrequencyPerYear(paymentFrequency: string): number {
    switch (paymentFrequency) {
      case 'accelerate-weekly':
        return 52;
      case 'weekly':
        return 52;
      case 'accelerate-bi-weekly':
        return 26;
      case 'bi-weekly':
        return 26;
      case 'semi-monthly':
        return 24;
      case 'monthly':
        return 12;
      case 'default':
        return 12;
    }
  }

  /**
   *
   * @param term
   * @param paymentFrequency
   */
  private calculateNumberOfPaymentPerTerm(term: number, paymentFrequency: string): number {
    const paymentFrequencyPerYear = this.getPaymentFrequencyPerYear(paymentFrequency);
    return term * paymentFrequencyPerYear;
  }

  /**
   *
   * @param mortgagePaymentMonthly
   * @param paymentFrequency
   */
  private estimateMortgagePaymentByMonthlyPayment(mortgagePaymentMonthly: number, paymentFrequency: string): number {
    const mortgagePaymentPerYear = mortgagePaymentMonthly * 12;
    switch (paymentFrequency) {
      case 'accelerate-weekly':
        return +(mortgagePaymentMonthly / 4).toFixed(2);
      case 'weekly':
        return +(mortgagePaymentPerYear / 52).toFixed(2);
      case 'accelerate-bi-weekly':
        return +(mortgagePaymentMonthly / 2).toFixed(2);
      case 'bi-weekly':
        return +(mortgagePaymentPerYear / 26).toFixed(2);
      case 'semi-monthly':
        return +(mortgagePaymentPerYear / 24).toFixed(2);
      case 'monthly':
        return mortgagePaymentMonthly;
    }
    return mortgagePaymentMonthly;
  }

}
