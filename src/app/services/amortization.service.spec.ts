import { TestBed } from '@angular/core/testing';

import { AmortizationService } from './amortization.service';

describe('AmortizationService', () => {
  let service: AmortizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AmortizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('#getAmortization should return correct amortization records', () => {
    const mortgageAmount = 100000;
    const monthlyPayment = 584.59;
    const numberOfPayments = 60;
    const interestRate = 5/1200; // Annual interest rate is 5%
    const results = service.getAmortization(mortgageAmount, monthlyPayment, numberOfPayments, interestRate);
    expect(results.length).toBe(60);
    const expectedFirstAmortizationRecord = {
      amortizationCapital: 167.92333333333335,
      amortizationInterest: 416.6666666666667,
      period: 1,
      remainingCapital: 99832.07666666666
    };
    const firstResult = results[0];
    expect(firstResult.amortizationInterest).toBe(expectedFirstAmortizationRecord.amortizationInterest);
    expect(firstResult.amortizationCapital).toBe(expectedFirstAmortizationRecord.amortizationCapital);
    expect(firstResult.period).toBe(expectedFirstAmortizationRecord.period);
    expect(firstResult.remainingCapital).toBe(expectedFirstAmortizationRecord.remainingCapital);
  });
});
