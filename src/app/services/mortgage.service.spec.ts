import { TestBed } from '@angular/core/testing';

import { MortgageService } from './mortgage.service';
import {Mortgage} from "../model/mortgage";

describe('MortgageService', () => {
  let service: MortgageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MortgageService);
  });

  it('should be created', () => {
      expect(service).toBeTruthy();
  });
  it('calculateMortgage should return correct mortgage data for monthly payment', () => {
      service.setUpData(100000,5, 25, 'monthly', 5, 0);
      const result = service.calculateMortgage();
      console.log(result);
      const expectedMortgage: Mortgage = {
        numberOfPaymentsPerTerm: 60,
        paymentPerTime: 584.59,
        prePaymentAmount: 0,
        totalInterestPaymentForTerm: 23655.5918824,
        principalPaymentForTerm: 11419.808117600001,
        totalCostForTerm: 35075.4,
        savingInterestForTerm: 0
      };
      expect(result.numberOfPaymentsPerTerm).toBe(expectedMortgage.numberOfPaymentsPerTerm);
      expect(result.paymentPerTime).toBe(expectedMortgage.paymentPerTime);
      expect(result.totalInterestPaymentForTerm).toBe(expectedMortgage.totalInterestPaymentForTerm);
      expect(result.principalPaymentForTerm).toBe(expectedMortgage.principalPaymentForTerm);
      expect(result.totalCostForTerm).toBe(expectedMortgage.totalCostForTerm);
      expect(result.savingInterestForTerm).toBe(expectedMortgage.savingInterestForTerm);
  });
});
