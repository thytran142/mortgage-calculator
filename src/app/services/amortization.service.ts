import { Injectable } from '@angular/core';
import {Amortization} from "../model/amortization";

@Injectable({
  providedIn: 'root'
})
export class AmortizationService {
  public amortizationTable: Amortization[] = [];
  public getAmortization(capital: number, pay: number, periods: number, interest: number): Amortization[] {
      // Make sure table is reset
      this.amortizationTable = [];
      this.updateAmortizationTable(capital, pay, periods, interest);
      return this.amortizationTable;
  }
  private updateAmortizationTable(capital: number, pay: number, periods: number, interest: number): void{
      const amortizationInterest = capital * (interest);
      let amortizationCapital = pay - amortizationInterest;
      if (capital < pay || periods == 1) {
        pay = capital + amortizationInterest;
        amortizationCapital = pay - amortizationInterest;
        this.updateTable(amortizationCapital, amortizationInterest, capital);
        return ;
      } else {
        this.updateTable(amortizationCapital, amortizationInterest, capital);
        this.updateAmortizationTable(capital - amortizationCapital, pay, periods - 1, interest);
      }
  }
  private updateTable(amortizationCapital: number, amortizationInterest: number, capital: number): void {
    this.amortizationTable.push({
      period: (this.amortizationTable.length + 1),
      amortizationInterest: amortizationInterest,
      amortizationCapital: amortizationCapital,
      remainingCapital: capital - amortizationCapital
    });
  }
}
